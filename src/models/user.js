// get an instance of mongoose and mongoose.Schema
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// set up a mongoose model and pass it using module.exports
/*
** @source - from where the email id is created. initially it may be facebook. 
** later if we want, we can add email or twitter or google plus for the source.
*/
module.exports = mongoose.model('User', new Schema({ 
	name: {
		first: String,
		last: String
	},
	email: { type: String, lowercase: true },
    username: String, 
    hashed_password: String, 
    source: String, 
    facebook: {},
    admin: Boolean,
    date_of_birth: Date,
    subsribe_for_updates: Boolean
}));