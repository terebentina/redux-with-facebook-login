import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import Helmet from 'react-helmet';
import * as authActions from 'redux/modules/auth';
import {bindActionCreators} from 'redux';

@connect(
  state => ({user: state.auth.user}), 
  dispatch => bindActionCreators({facebookLogin}, dispatch))

class FacebookLogin extends Component {   
  static propTypes = {
    user: PropTypes.object,
    facebookLogin: PropTypes.func
  } 

  render() {
    const {user, facebookLogin} = this.props;
    const styles = require('./FacebookLogin.scss');
    return (
      <div className={styles.facebookLogin + ' container'}>
        <Helmet title="FacebookLogin"/>
        <button type="submit" onClick={facebookLogin}>Login With Facebook</button>  
      </div>
    )
  };  
  
}

export default FacebookLogin;
